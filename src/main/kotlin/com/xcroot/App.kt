import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.stage.Stage

class App : Application() {
    override fun start(primaryStage: Stage?) {
        var loader = FXMLLoader(javaClass.classLoader.getResource("Main.fxml"))
        var root = loader.load<Parent>()
        primaryStage?.title = "Jdattobd - Linux系统可视化备份"
        primaryStage?.isResizable = false
        primaryStage?.isMaximized = false
        primaryStage?.icons?.add(Image("/UI/backup.png"))
        primaryStage?.scene = Scene(root)
        primaryStage?.show()
    }
}

fun main(args: Array<String>) {
    Application.launch(App::class.java, *args)
}