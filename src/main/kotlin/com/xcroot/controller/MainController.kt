package com.xcroot.controller

import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Alert
import javafx.scene.control.ComboBox
import javafx.scene.control.TextField
import javafx.scene.layout.AnchorPane
import javafx.stage.FileChooser
import java.net.URL
import java.util.*
import kotlin.system.exitProcess

class MainController : Initializable {
    @FXML
    private lateinit var apRoot: AnchorPane
    @FXML
    private lateinit var savePath: TextField
    @FXML
    private lateinit var deviceSelect:ComboBox<String>
    override fun initialize(location: URL?, resources: ResourceBundle?) {
        //check system, ensure linux
        val osName = System.getProperty("os.name")
        if(!osName.endsWith("Linux")){
            var alert = Alert(Alert.AlertType.WARNING)
            alert.title = "Sorry"
            alert.contentText = "不支持当前系统：$osName\n 请使用Linux系列系统！"
            alert.showAndWait()
            exitProcess(0)
        }

        //load disk list
        deviceSelect.selectionModel.select("C:")
        deviceSelect.items.add("C:")
    }

    @FXML
    fun selectSavePath(actionEvent: ActionEvent) {
        val stage = apRoot.scene.window
        val saveFilseChooser = FileChooser()
        saveFilseChooser.title = "选择镜像保存位置"
        val extFilter = FileChooser.ExtensionFilter("IMG files (*.img)", "*.img")
        saveFilseChooser.extensionFilters.add(extFilter)
        val file = saveFilseChooser.showSaveDialog(stage)
        if (file != null)
            savePath.text = file.path
    }
}